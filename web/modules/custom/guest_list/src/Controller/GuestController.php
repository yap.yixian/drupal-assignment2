<?php

namespace Drupal\guest_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GuestController extends ControllerBase {
    private $table;
    private $baseUrl;

    public function __construct() {
        $config = \Drupal::config('guest_list.settings');
        
        $this->table = $config->get('guest_list_table');
        $this->baseUrl = $config->get('base_url');
    }

    public function viewGuest() {
        $database = \Drupal::service('database');

        $guests = $database->select($this->table, 'gl')->fields('gl')->execute()->fetchAll();
        
        return [
            '#theme' => 'view_guest',
            '#guests' => $guests,
            '#baseUrl' => $this->baseUrl,
        ];
    }

    public function deleteGuest($id) {
        $database = \Drupal::service('database');

        $database->delete($this->table)->condition('id', $id)->execute();
        
        $path = \Drupal\Core\Url::fromRoute('guest_list.view_guest')->toString();
        $response = new RedirectResponse($path);
        $response->send();
    }
}