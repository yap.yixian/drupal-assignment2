<?php

namespace Drupal\guest_list\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;  

class BlockMessageForm extends ConfigFormBase {
    protected function getEditableConfigNames() {  
        return [  
           'guest_list.adminsettings',  
        ];  
    }
      
    public function getFormId() {  
        return 'block_message_form';  
    }

    public function buildForm(array $form, FormStateInterface $form_state) {  
        // $config = $this->config('guest_list.adminsettings');  
        $config = $this->config('guest_list.settings');  
      
        $form['message'] = [  
          '#type' => 'textarea',  
          '#title' => $this->t('Block Message'),  
          '#default_value' => $config->get('block_message'),  
        ];  
      
        return parent::buildForm($form, $form_state);  
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {  
        parent::submitForm($form, $form_state);  
      
        $config = \Drupal::service('config.factory')->getEditable('guest_list.settings');
        $config->set('block_message', $form_state->getValue('message'))->save();

        // $this->config('guest_list.adminsettings')  
        //   ->set('block_message', $form_state->getValue('message'))  
        //   ->save();  
    }
}