<?php

namespace Drupal\guest_list\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CreateGuestForm extends FormBase {
    private $table;

    public function __construct() {
        $config = \Drupal::config('guest_list.settings');
        
        $this->table = $config->get('guest_list_table');
    }

    public function getFormId() {
        return 'create_guest_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['first_name'] = [
            '#title' => t('First Name'),
            '#type' => 'textfield',
            '#required' => true,
            '#maxlength' => 255
        ];
        $form['last_name'] = [
            '#title' => t('Last Name'),
            '#type' => 'textfield',
            '#required' => true,
            '#maxlength' => 255
        ];
        $form['email'] = [
            '#title' => t('Email'),
            '#type' => 'email',
            '#required' => true,
            '#maxlength' => 255
        ];
        $form['phone'] = [
            '#title' => t('Phone'),
            '#type' => 'textfield',
            '#required' => true,
            '#maxlength' => 255,
        ];
        $form['contact_type'] = [
            '#title' => t('Contact Type'),
            '#type' => 'radios',
            '#required' => true,
            '#options' => [
                'home' => $this->t('Home'),
                'office' => $this->t('Office'),
            ],
        ];
        $form['approved'] = [
            '#title' => t('Approved'),
            '#type' => 'radios',
            '#required' => true,
            '#options' => [
                0 => $this->t('No'),
                1 => $this->t('Yes'),
            ],
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Create')
        ];
        $form['#theme'] = 'create_guest';

        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
        $fields = ['email', 'phone'];

        for($i=0; $i < count($fields); $i++) { 
            $value = $form_state->getValue($fields[$i]);
            switch ($fields[$i]) {
                case 'email':
                    if(!\Drupal::service('email.validator')->isValid($value)) {
                        $form_state->setErrorByName('email', t('%mail is not valid.', [
                            '%mail' => $value
                        ]));
                        return;
                    }
                    if(!\Drupal::service('guest_list.form_validator_service')->isUnique('email', $value)) {
                        $form_state->setErrorByName('email', t('%mail is not unique.', [
                            '%mail' => $value
                        ]));
                        return;
                    }
                case 'phone':
                    if(!\Drupal::service('guest_list.form_validator_service')->isUnique('phone', $value)) {
                        $form_state->setErrorByName('phone', t('%phone is not unique.', [
                            '%phone' => $value
                        ]));
                        return;
                    }
            }
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

        $connection = \Drupal::service('database');

        $result = $connection->insert($this->table)
                ->fields([
                    'uid' => $user->id(),
                    'first_name' => $form_state->getValue('first_name'),
                    'last_name' => $form_state->getValue('last_name'),
                    'email' => $form_state->getValue('email'),
                    'phone' => $form_state->getValue('phone'),
                    'contact_type' => $form_state->getValue('contact_type'),
                    'approved' => $form_state->getValue('approved'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ])
                ->execute();
        \Drupal::messenger()->addStatus('Guest created');
    }
}