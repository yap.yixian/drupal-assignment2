<?php

namespace Drupal\guest_list\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class EditGuestForm extends FormBase {
    private $table;
    public $selectedGuestId;
    public $selectedEmail;
    public $selectedPhone;

    public function __construct() {
        $config = \Drupal::config('guest_list.settings');
        
        $this->table = $config->get('guest_list_table');
    }

    public function getFormId() {
        return 'edit_guest_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $guestId = NULL) {
        // Retrieve guest
        $database = \Drupal::service('database');

        $guest = $database->select($this->table, 'gl')->fields('gl')->condition('id', $guestId)->execute()->fetchAll();
        if(count($guest) > 0) {
            $guest = $guest[0];

            $this->selectedGuestId = $guest->id;
            $this->selectedEmail = $guest->email;
            $this->selectedPhone = $guest->phone;
            
            $form['first_name'] = [
                '#title' => t('First Name'),
                '#type' => 'textfield',
                '#required' => true,
                '#maxlength' => 255,
                '#default_value' => $guest->first_name,
            ];
            $form['last_name'] = [
                '#title' => t('Last Name'),
                '#type' => 'textfield',
                '#required' => true,
                '#maxlength' => 255,
                '#default_value' => $guest->last_name,
            ];
            $form['email'] = [
                '#title' => t('Email'),
                '#type' => 'email',
                '#required' => true,
                '#maxlength' => 255,
                '#default_value' => $guest->email,
            ];
            $form['phone'] = [
                '#title' => t('Phone'),
                '#type' => 'textfield',
                '#required' => true,
                '#maxlength' => 255,
                '#default_value' => $guest->phone,
            ];
            $form['contact_type'] = [
                '#title' => t('Contact Type'),
                '#type' => 'radios',
                '#required' => true,
                '#default_value' => $guest->contact_type,
                '#options' => [
                    'home' => $this->t('Home'),
                    'office' => $this->t('Office'),
                ],
            ];
            $form['approved'] = [
                '#title' => t('Approved'),
                '#type' => 'radios',
                '#required' => true,
                '#default_value' => $guest->approved,
                '#options' => [
                    0 => $this->t('No'),
                    1 => $this->t('Yes'),
                ],
            ];
            $form['submit'] = [
                '#type' => 'submit',
                '#value' => t('Update')
            ];
            $form['#theme'] = 'edit_guest';
    
            return $form;
        } else {
            return [
                '#markup' => 'Guest Not Found'
            ];
        }
    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
        // Validate
        $fields = ['email', 'phone'];

        for($i=0; $i < count($fields); $i++) { 
            $value = $form_state->getValue($fields[$i]);
            switch ($fields[$i]) {
                case 'email':
                    if(!\Drupal::service('email.validator')->isValid($value)) {
                        $form_state->setErrorByName('email', t('%mail is not valid.', [
                            '%mail' => $value
                        ]));
                        return;
                    }
                    if($value != $this->selectedEmail && !\Drupal::service('guest_list.form_validator_service')->isUnique('email', $value)) {
                        $form_state->setErrorByName('email', t('%mail is not unique.', [
                            '%mail' => $value
                        ]));
                        return;
                    }
                case 'phone':
                    if($value != $this->selectedPhone && !\Drupal::service('guest_list.form_validator_service')->isUnique('phone', $value)) {
                        $form_state->setErrorByName('phone', t('%phone is not unique.', [
                            '%phone' => $value
                        ]));
                        return;
                    }
            }
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $connection = \Drupal::service('database');

        $result = $connection->update($this->table)
                ->fields([
                    'first_name' => $form_state->getValue('first_name'),
                    'last_name' => $form_state->getValue('last_name'),
                    'email' => $form_state->getValue('email'),
                    'phone' => $form_state->getValue('phone'),
                    'contact_type' => $form_state->getValue('contact_type'),
                    'approved' => $form_state->getValue('approved'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ])
                ->condition('id', $this->selectedGuestId)
                ->execute();
        \Drupal::messenger()->addStatus('Guest edited');
    }
}