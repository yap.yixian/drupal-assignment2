<?php

namespace Drupal\guest_list;

class FormValidatorService {
    private $table;

    public function __construct() {
        $config = \Drupal::config('guest_list.settings');
        
        $this->table = $config->get('guest_list_table');
    }

    public function isUnique($field, $value) {
        $database = \Drupal::service('database');

        $number = $database->select($this->table)
                ->condition($field, $value)
                ->countQuery()
                ->execute()
                ->fetchField();

        if($number > 0) {
            return false;    
        }
        return true;

    }
}