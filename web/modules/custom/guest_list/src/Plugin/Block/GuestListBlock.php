<?php

namespace Drupal\guest_list\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *  id = "guest_list_block",
 *  admin_label = @Translation("Guest List Block"),
 *  category = @Translation("Blocks")
 * )
 */

class GuestListBlock extends BlockBase {
    public function build() {
        // $config = \Drupal::config('guest_list.adminsettings');
        $config = \Drupal::config('guest_list.settings');

        return [
            '#markup' => $config->get('block_message'),
        ];
    }
}